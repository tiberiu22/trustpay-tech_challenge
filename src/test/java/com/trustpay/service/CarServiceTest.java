package com.trustpay.service;

import com.trustpay.domain.CarDO;
import com.trustpay.domain.CarType;
import com.trustpay.domain.StorageRepository;
import com.trustpay.exceptions.AlreadyInUseException;
import com.trustpay.exceptions.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class CarServiceTest {

    @Configuration
    static class CarServiceTestConfiguration {
        @Bean
        public CarService serviceBeanConfig() {
            return new CarService();
        }

        @Bean
        public StorageRepository storageRepository() {
            return new StorageRepository();
        }
    }

    @Autowired
    private CarService carService;

    @Autowired
    private StorageRepository storageRepository;


    @Before
    public void clearMemoryCache() {
        storageRepository.clearCache();
    }

    @Test
    public void newCarTest() {
        CarDO carDO = carService.createNewCar("test", "model", CarType.CAR, 4);
        assertNotNull(carDO);
        assertNotNull(carDO.getId());

        // check that the new car exists in storage
        CarDO carDO1 = storageRepository.getCar(carDO.getId());
        assertNotNull(carDO1);
        assertEquals(carDO.getId(), carDO1.getId());
    }

    @Test
    public void deleteCarTest() throws NotFoundException {
        // create a new car
        CarDO carDO = carService.createNewCar("test", "model", CarType.CAR, 4);
        assertNotNull(carDO);
        assertNotNull(carDO.getId());

        carService.deleteCar(carDO.getId());
        CarDO carDO1 = storageRepository.getCar(carDO.getId());

        assertNull(carDO1);
    }


    @Test
    public void testRentedCar() throws NotFoundException, AlreadyInUseException {
        // create a new car
        CarDO carDO = carService.createNewCar("test", "model", CarType.CAR, 4);
        assertNotNull(carDO);
        assertNotNull(carDO.getId());

        // rent the car
        Long carId = carDO.getId();
        CarDO carDO1 = carService.rentCar(carId, "test@test.com");
        assertEquals(carId, carDO1.getId());
        assertTrue(carDO1.getRentedBy() != null);
        assertEquals("test@test.com", carDO1.getRentedBy().getEmail());
    }

    @Test
    public void getAllAvailableCars() throws NotFoundException, AlreadyInUseException {
        // create 5 cars
        for (int i =1; i <= 5; i++) {
            carService.createNewCar("test", "model", CarType.CAR, 4);
        }

        List<CarDO> cars = carService.getAllAvailableCars();
        assertEquals(5, cars.size());

        // rent the first car in the list
        Long carId = cars.get(0).getId();
        carService.rentCar(carId, "test@test.com");

        // test that we only have 4 available cars remaining
        cars = carService.getAllAvailableCars();
        assertEquals(4, cars.size());
    }

    @Test
    public void getAllRentedCars() throws NotFoundException, AlreadyInUseException {
        // create 5 cars
        Long carId = 0L;
        for (int i =1; i <= 5; i++) {
            CarDO carDO = carService.createNewCar("test", "model", CarType.CAR, 4);
            carId = carDO.getId();
        }

        List<CarDO> cars = carService.getAllRentedCars();
        assertEquals(0, cars.size());

        // rent the first car in the list
        carService.rentCar(carId, "test@test.com");

        // test that we only have 4 available cars remaining
        cars = carService.getAllRentedCars();
        assertEquals(1, cars.size());
    }

}
