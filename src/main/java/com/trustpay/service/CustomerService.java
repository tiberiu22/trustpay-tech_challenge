package com.trustpay.service;

import com.trustpay.domain.CustomerDO;
import com.trustpay.domain.StorageRepository;
import com.trustpay.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private StorageRepository storageRepository;

    public List<CustomerDO> getAllClients() {
        return storageRepository.getAllCustomers();
    }

    public CustomerDO getCustomer(String email) throws NotFoundException {
        CustomerDO customerDO = storageRepository.getCustomerByEmail(email);

        if (customerDO == null) {
            throw new NotFoundException("Customer not found");
        }

        return customerDO;
    }
}
