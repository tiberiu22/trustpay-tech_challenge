package com.trustpay.service;

import com.trustpay.domain.CarDO;
import com.trustpay.domain.StorageRepository;
import com.trustpay.domain.CarType;
import com.trustpay.exceptions.AlreadyInUseException;
import com.trustpay.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService {

    @Autowired
    StorageRepository storageRepository;

    public CarDO createNewCar(
        String brand,
        String model,
        CarType carType,
        int numberOfSeats) {

        CarDO carDO = new CarDO();
        carDO.setId(storageRepository.getLastCarId());
        carDO.setBrand(brand);
        carDO.setModelName(model);
        carDO.setCarType(carType);
        carDO.setNumberOfSeats(numberOfSeats);
        carDO.setNumberOfWheels(carType.getNumberOfWheels());

        storageRepository.saveCar(carDO);
        return carDO;
    }

    public void deleteCar(Long carId) throws NotFoundException {
        CarDO carDO = storageRepository.getCar(carId);

        if (carDO == null) {
            throw new NotFoundException("Car not found");
        }

        // Remove the car from the renter if it was rented by someone
        if (carDO.getRentedBy() != null) {
            storageRepository.finishRental(carDO.getRentedBy().getEmail(), carId);
        }

        storageRepository.deleteCar(carId);
    }

    public List<CarDO> getAllRentedCars() {
        return storageRepository.getAllRentedCars();
    }

    public List<CarDO> getAllAvailableCars() {
        return storageRepository.getAllAvailableCars();
    }

    public CarDO rentCar(Long carId, String clientEmail) throws NotFoundException, AlreadyInUseException {
        return storageRepository.rentCar(carId, clientEmail);
    }

    public void terminateRental(Long carId, String clientEmail) {
        storageRepository.finishRental(clientEmail, carId);
    }
}
