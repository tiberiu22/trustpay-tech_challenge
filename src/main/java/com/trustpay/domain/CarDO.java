package com.trustpay.domain;


import lombok.Data;

@Data
public class CarDO {

    private Long id;
    private String brand;
    private String modelName;
    private CarType carType;
    private int numberOfWheels = 4;
    private int numberOfSeats = 2;
    private CustomerDO rentedBy;
}
