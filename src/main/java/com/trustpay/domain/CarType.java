package com.trustpay.domain;

public enum  CarType {

    CAR,
    VAN,
    CONVERTIBLE,
    MOTORBIKE {

        @Override
        public int getNumberOfWheels() {
            return 2;
        }
    }
    ;

    public int getNumberOfWheels() {
        return 4;
    }
}
