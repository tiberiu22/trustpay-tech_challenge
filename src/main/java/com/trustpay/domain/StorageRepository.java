package com.trustpay.domain;

import com.trustpay.exceptions.AlreadyInUseException;
import com.trustpay.exceptions.NotFoundException;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Component class that plays the role of the repository and stores a
 * cache of cars and clients. Combined the potential 2 repositories for
 * clients and cars in order to simplify the logic and locking mechanism
 * when handling transactions that involve both caches.
 */
@Component
public class StorageRepository {

    private Map<Long, CarDO> carCache = new HashMap<>();
    private Long lastUsedCarId = 0L;

    private Map<String, CustomerDO> customerCache = new HashMap<>();

    public CarDO getCar(Long id) {
        synchronized (carCache) {
            return carCache.get(id);
        }
    }

    public void deleteCar(Long id) {
        synchronized (carCache) {
            carCache.remove(id);
        }
    }

    public void saveCar(CarDO carDO) {
        if (carDO.getId() == null) {
            throw new NullPointerException("The car you're trying to save is missing it's id");
        }

        synchronized (carCache) {
            carCache.put(carDO.getId(), carDO);
        }
    }

    public List<CarDO> getAllRentedCars() {
        List<CarDO> rentedCars = new ArrayList<>();
        synchronized (carCache) {
            rentedCars = carCache.values().stream()
                .filter(carDO ->  carDO.getRentedBy() != null)
                .collect(Collectors.toList());
        }

        return rentedCars;
    }

    public List<CarDO> getAllAvailableCars() {
        List<CarDO> availableCars = new ArrayList<>();
        synchronized (carCache) {
            availableCars = carCache.values().stream()
                .filter(carDO ->  carDO.getRentedBy() == null)
                .collect(Collectors.toList());
        }

        return availableCars;

    }

    public Long getLastCarId() {
        Long returnValue;
        synchronized (lastUsedCarId) {
            returnValue = lastUsedCarId+1;
            lastUsedCarId++;
        }

        return returnValue;
    }

    public CarDO rentCar(Long carId, String clientEmail) throws NotFoundException, AlreadyInUseException {
        synchronized (carCache) {
            CarDO carDO = getCar(carId);
            if (carDO == null) {
                throw new NotFoundException("Car not found exception!");
            }
            // check that the car is not rented already by another user
            if (carDO.getRentedBy() != null) {
                throw new AlreadyInUseException("Car is already rented out");
            }

            CustomerDO customerDO = getOrCreateCustomer(clientEmail);

            customerDO.getRentedCars().add(carDO);
            carDO.setRentedBy(customerDO);

            return carDO;
        }
    }

    public void finishRental(String clientEmail, Long carId) {
        synchronized (carCache) {
            synchronized (customerCache) {
                CustomerDO customerDO = getOrCreateCustomer(clientEmail);
                ListIterator<CarDO> iterator = customerDO.getRentedCars().listIterator();
                while (iterator.hasNext()) {
                    CarDO carDO = iterator.next();
                    if (carDO.getId().equals(carId)) {
                        iterator.remove();
                        carDO.setRentedBy(null);
                        carCache.put(carId, carDO);
                        break;
                    }
                }
            }
        }
    }

    private CustomerDO getOrCreateCustomer(String clientEmail) {
        synchronized (customerCache) {
            return Optional.ofNullable(customerCache.get(clientEmail))
                    .orElseGet(() -> {
                        CustomerDO customer = new CustomerDO(clientEmail);
                        customerCache.put(clientEmail, customer);
                        return customer;
                    });
        }
    }

    public List<CustomerDO> getAllCustomers() {
        synchronized (customerCache) {
            return customerCache.values().stream().collect(Collectors.toList());
        }
    }

    public CustomerDO getCustomerByEmail(String email) {
        synchronized (customerCache) {
            return customerCache.get(email);
        }
    }

    public void clearCache() {
        customerCache.clear();
        carCache.clear();
    }
}
