package com.trustpay.domain;

import lombok.Data;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

@Data
public class CustomerDO {

    @NonNull
    private String email;
    private List<CarDO> rentedCars = new ArrayList<>();

}
