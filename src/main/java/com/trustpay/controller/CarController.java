package com.trustpay.controller;

import com.trustpay.controller.mapper.CarMapper;
import com.trustpay.controller.models.CarDTO;
import com.trustpay.domain.CarDO;
import com.trustpay.exceptions.AlreadyInUseException;
import com.trustpay.exceptions.NotFoundException;
import com.trustpay.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cars")
public class CarController {

    @Autowired
    private CarService carService;

    @GetMapping("/rented")
    public List<CarDTO> getRentedCars() {
        return carService.getAllRentedCars().stream()
            .map(CarMapper::toDTO)
            .collect(Collectors.toList());
    }

    @GetMapping("/available")
    public List<CarDTO> getAvailableCars() {
        return carService.getAllAvailableCars().stream()
            .map(CarMapper::toDTO)
            .collect(Collectors.toList());
    }

    @PostMapping
    public CarDTO createNewCar(@RequestBody CarDTO car) {
        CarDO carDO = carService.createNewCar(
            car.getBrand(),
            car.getModelName(),
            car.getCarType(),
            car.getNumberOfSeats());

        return CarMapper.toDTO(carDO);
    }

    @DeleteMapping("/{carId}/")
    public void deleteCar(@PathVariable Long carId) throws NotFoundException {
        carService.deleteCar(carId);
    }

    @PostMapping("/{carId}/rent")
    public CarDTO rentCar(@PathVariable Long carId, @RequestParam("clientEmail")String clientEmail) throws NotFoundException, AlreadyInUseException {
        CarDO carDO = carService.rentCar(carId, clientEmail);
        return CarMapper.toDTO(carDO);
    }

    @DeleteMapping("/{carId}/rent")
    public void finishRental(@PathVariable Long carId, @RequestParam("clientEmail")String clientEmail) {
        carService.terminateRental(carId, clientEmail);
    }

    /** Since it wasn't required in the requirements will ignore an update method for
     *  cars.
      */
}
