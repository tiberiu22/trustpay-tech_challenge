package com.trustpay.controller.mapper;

import com.trustpay.controller.models.CustomerDTO;
import com.trustpay.domain.CustomerDO;

import java.util.List;
import java.util.stream.Collectors;

public class CustomerMapper {

    public static CustomerDTO toDTO(CustomerDO customerDO) {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setEmail(customerDO.getEmail());
        customerDTO.setRentedCars(customerDO.getRentedCars().stream().map(CarMapper::toDTO).collect(Collectors.toList()));

        return customerDTO;
    }

    public static List<CustomerDTO> toDTOList(List<CustomerDO> customers) {
        return customers.stream()
            .map(CustomerMapper::toDTO)
            .collect(Collectors.toList());
    }
}
