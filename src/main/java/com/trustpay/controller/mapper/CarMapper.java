package com.trustpay.controller.mapper;

import com.trustpay.controller.models.CarDTO;
import com.trustpay.domain.CarDO;

public class CarMapper {

    public static CarDTO toDTO(CarDO carDO) {
        CarDTO carDTO = new CarDTO();
        carDTO.setId(carDO.getId());
        carDTO.setBrand(carDO.getBrand());
        carDTO.setModelName(carDO.getModelName());
        carDTO.setCarType(carDO.getCarType());
        carDTO.setNumberOfSeats(carDO.getNumberOfSeats());
        carDTO.setNumberOfWheels(carDO.getNumberOfWheels());
        carDTO.setRented(carDO.getRentedBy() != null);

        return carDTO;
    }

    public static CarDO toDO(CarDTO carDTO) {
        CarDO carDO = new CarDO();
        carDO.setId(carDTO.getId());
        carDO.setBrand(carDTO.getBrand());
        carDO.setModelName(carDTO.getModelName());
        carDO.setCarType(carDTO.getCarType());
        carDO.setNumberOfSeats(carDTO.getNumberOfSeats());
        carDO.setNumberOfWheels(carDTO.getNumberOfWheels());

        return carDO;
    }
}
