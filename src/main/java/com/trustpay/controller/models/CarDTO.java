package com.trustpay.controller.models;

import com.trustpay.domain.CarType;
import lombok.Data;

@Data
public class CarDTO {

    private Long id;
    private String brand;
    private String modelName;
    private CarType carType;
    private int numberOfWheels = 4;
    private int numberOfSeats = 2;
    private boolean isRented;

}
