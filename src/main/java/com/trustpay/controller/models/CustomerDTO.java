package com.trustpay.controller.models;

import com.trustpay.domain.CarDO;
import lombok.Data;

import java.util.List;

@Data
public class CustomerDTO {

    private String email;
    private String firstName;
    private String lastName;
    private List<CarDTO> rentedCars;

}
