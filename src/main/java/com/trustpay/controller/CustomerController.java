package com.trustpay.controller;

import com.trustpay.controller.mapper.CustomerMapper;
import com.trustpay.controller.models.CustomerDTO;
import com.trustpay.domain.CustomerDO;
import com.trustpay.exceptions.NotFoundException;
import com.trustpay.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {


    @Autowired
    private CustomerService customerService;

    @GetMapping
    public List<CustomerDTO> getAllCustomers() {
        return CustomerMapper.toDTOList(customerService.getAllClients());
    }

    @GetMapping("/search")
    public CustomerDTO searchForCustomer(@RequestParam("customerEmail") String customerEmail) throws NotFoundException {
        CustomerDO customerDO = customerService.getCustomer(customerEmail);

        return CustomerMapper.toDTO(customerDO);
    }

}
