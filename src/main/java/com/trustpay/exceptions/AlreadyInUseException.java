package com.trustpay.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class AlreadyInUseException extends Exception {

    public AlreadyInUseException(String message) {
        super(message);
    }
}
