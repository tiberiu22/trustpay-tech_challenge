package com.trustpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.context.WebApplicationContext;

@SpringBootApplication
public class WebApplicationTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebApplicationTestApplication.class, args);
	}
}
